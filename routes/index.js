var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/hook', async function(req, res, next) {
  console.log('branch: ', req.body.ref);
  console.log('username: ', req.body.user_name);

  const exec = require("child_process").exec;
  await exec("cd /var/www/html/test && git pull origin master", (error, stdout, stderr) => {
    //do whatever here
  })

  res.json({ response: res.body });

});

module.exports = router;
